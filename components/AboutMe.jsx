import React, { useContext } from "react";
import content from "../lib/content";
import { motion } from "framer-motion";
import ThemeContext from "../theme/ThemeContext";

const AboutMe = () => {
	const { dark, toggleDark } = useContext(ThemeContext);
	return (
		<div className="w-10/12 text-lg mx-auto text-justify md:text-2xl tracking-wide dark:text-white">
			
			<div className="w-10/12 mx-auto flex flex-col md:flex-row-reverse items-center justify-between -mt-24">
				<div className="w-8/12 md:w-3/12 -mt-20">
					<motion.button
						onClick={() => toggleDark()}
						className="focus:outline-none"
						whileTap={{ scale: 0.98 }}
						whileHover={{ scale: 1.04 }}
					>
						<img
							className="rounded-full w-full"
							effect="blur"
							src={content.aboutsaya.img.url}
							alt={content.aboutsaya.img.alt}
						/>
					</motion.button>
				</div>
				<div className="text-black dark:text-white text-center md:text-left -mt-8">

					<h2 className="text-3xl md:text-5xl font-bold md:mt-10">
						About <span className="text-blue">Me</span>
					</h2>
					<div style={{ height: "5vh" }}></div>

					{content.about.map((line, index) => {
						return (
							<p key={index}>
								{line}
								<p></p>
							</p>
						);
					})}
					
				</div>
				
				
			</div>

			<div className="w-10/12 mx-auto flex flex-col md:flex-row-reverse items-center justify-between ">
				<div className="w-8/12 md:w-3/12 -mt-20">
					
				</div>
				<div className="text-black dark:text-white text-center md:text-left ">

						<h2 className="text-3xl md:text-5xl font-bold md:mt-10">
							My <span className="text-blue">Hobby</span>
						</h2>
						<div style={{ height: "2vh" }}></div>

						<div className="grid grid-cols-3 gap-4 md:grid-cols-4 md:gap-6 lg:grid-cols-10">
							{content.hobiku.map((tech, index) => {
								return (
									<motion.div whileHover={{ scale: 1.1 }} key={index} className="m-auto">
										<span className="bg-white shadow-lg m-2 rounded-full">
											<img className="h-18 md:h-24" src={tech.img} alt={tech.name} />
											<h4 className="mt-1 text-sm text-darkgrey dark:text-grey">{tech.name}</h4>
										</span>
									</motion.div>
								);
							})}
						</div>
					
				</div>
				
				
			</div>

			

		

		</div>
	);
};

export default AboutMe;
