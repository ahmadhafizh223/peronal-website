import React from 'react'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const Arrow = () => {
    return (
			<div>
				<FontAwesomeIcon icon={faCoffee}></FontAwesomeIcon>
			</div>
    )
}

export default Arrow;
