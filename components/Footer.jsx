import React from "react";

const Footer = () => {
	return (
		<div className="text-darkgrey py-3 bg-white dark:bg-black dark:text-grey">
			<div className="flex justify-center w-11/12 mx-auto mt-1 items-center text-sm">
				© {new Date().getFullYear()} | 
				<a href="https://gitlab.com/ahmadhafizh223" className="ml-1">Ahmad Hafizh Al-Fauzi</a>
				
			</div>
		</div>
	);
};

export default Footer;
