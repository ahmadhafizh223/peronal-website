import React, { useContext } from "react";
import Link from "next/link";
import Typical from "react-typical";
import { motion } from "framer-motion";
import ThemeContext from "../theme/ThemeContext";
import content from "../lib/content";

const Landing = () => {
	const { dark, toggleDark } = useContext(ThemeContext);
	return (
		<div className="flex items-center justify-center h-screen dark:bg-black">
			<div className="w-10/12 mx-auto flex flex-col md:flex-row-reverse items-center justify-between">
				<div className="w-8/12 md:w-3/12 -mt-20">
					<motion.button
						onClick={() => toggleDark()}
						className="focus:outline-none"
						whileTap={{ scale: 0.98 }}
						whileHover={{ scale: 1.04 }}
					>
						<img
							className="rounded-full w-full"
							effect="blur"
							src={content.landing.img.url}
							alt={content.landing.img.alt}
						/>
					</motion.button>
				</div>
				<div className="text-black dark:text-white text-center md:text-left">
					<h2 className="text-3xl md:text-5xl font-bold">
						{content.landing.text[0]}
						<br />
						{content.landing.text[1]} <span className="text-blue">{content.landing.text[2]}</span>
					</h2>
					<h1 className="text-grey text-xl">
						{content.landing.text[3]}{" "}
						<Typical
							steps={content.landing.typical}
							loop={Infinity}
							className="inline-block text-grey"
						/>
					</h1>
					<Link href="/contact">
						<motion.button
							whileHover={{ backgroundColor: "#2B8BFC", color: "#FFFFFF" }}
							whileTap={{ scale: 0.95, backgroundColor: "#2B8BFC", color: "#FFFFFF" }}
							className="bg-lightblue dark:bg-blue px-10 py-3 md:px-24 mt-10 rounded-lg text-blue dark:text-white font-medium capitalize focus:outline-none"
						>
							{content.landing.btnText.toUpperCase()}
						</motion.button>
					</Link>
				</div>
			</div>
		</div>
	);
};

export default Landing;
