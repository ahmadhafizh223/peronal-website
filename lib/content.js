export default {
	nav: {
		logo: "HFZ",
		links: [
			{
				text: "ABOUT",
				to: "/about",
			},
			{
				text: "EXPERIENCE",
				to: "/work",
			},
			{
				text: "CONTACT",
				to: "/contact",
			},
		],
	},
	landing: {
		img: {
			url: "assets/hafizh3.png",
			alt: "Ahmad Hafizh",
		},
		text: ["Hi IT Paragon ! 👋", "I'm", "Ahmad Hafizh Al-Fauzi.", "I'm a"],
		typical: [
			"Graduate of Electrical Engineering 🎓",
			2000,
			"Software Engineer 💻",
			2000,
			"Technology Enthusiast 🤖",
			2000,
			"ESFP 👨",
			2000,
		],
		btnText: "Get in Touch",
	},

	aboutsaya: {
		img: {
			url: "assets/hafizh.png",
			alt: "Ahmad Hafizh",
		},
		text: [
			"My name is Ahmad Hafizh Al Fauzi.",
			"",
			"I was born in Tulungaguagung, November 11, 1996.",
			"",
			"Personality Type : ESFP",
		],

	},

	about: [
		"My name is Ahmad Hafizh Al Fauzi 👨.",
		"",
		"I was born in Tulungaguagung, November 11, 1996  👶.",
		"",
		"Personality Type : ESFP ✨.",
		"",
		"Favorite food and drink : Sego Pecel 🍛 & Teh Anget 🍵.",
		"",
		"Favorite series or movie : Alice in Borderland 📽️."
	],
	techstack: [
		{
			img: "/assets/stack/flutter.svg",
			name: "Flutter",
		},
		{
			img: "/assets/stack/kotlin.svg",
			name: "Kotlin",
		},
		{
			img: "/assets/stack/go.svg",
			name: "Go",
		},
		{
			img: "/assets/stack/fastapi.svg",
			name: "FastAPI",
		},
		{
			img: "/assets/stack/python.svg",
			name: "Python",
		},
		{
			img: "/assets/stack/react.svg",
			name: "React",
		},
		{
			img: "/assets/stack/vue.svg",
			name: "Vue",
		},
		{
			img: "/assets/stack/nextjs.svg",
			name: "Next JS",
		},
		{
			img: "/assets/stack/nodejs.svg",
			name: "Node",
		},
	],
	hobiku: [
		{
			img: "/assets/hobi/minton.png",
			name: "Badminton",
		},
		{
			img: "/assets/hobi/mobile-legends.png",
			name: "E-sport",
		},
		{
			img: "/assets/hobi/netflix.png",
			name: "Watching Movie",
		},

	],
	contact: {
		heading: ["Get in ", "Touch"],
		primary: [

			{
				img: "/assets/contact/email.svg",
				link: "mailto:ahmadhafizh223@gmail.com",
				name: "Mail",
			},
		],
		social: [
			{
				img: "/assets/contact/Github.svg",
				link: "https://gitlab.com/ahmadhafizh223",
				name: "GitLab",
			},
			{
				img: "/assets/contact/ig.png",
				link: "https://instagram.com/sidv_22",
				name: "Instagram",
			},
			{
				img: "/assets/contact/Twitter.svg",
				link: "https://twitter.com/sidv_22",
				name: "Twitter",
			},
			{
				img: "/assets/contact/LinkedIN.svg",
				link: "https://linkedin.com/in/siddharthav22/",
				name: "LinkedIN",
			},
		],
	},
	resume: {
		file: "/assets/resume/cv_hafizh.pdf",
		btn: "DOWNLOAD RESUME",
	},
	projects: [
		{
			title: "Google Tasks CLI",
			subtitle: "Manage your Google Tasks right from your terminal.",
			img: "assets/projects/gtasks.svg",
			redirect: "https://asciinema.org/a/372759",
			github: "https://github.com/BRO3886/google-tasks-cli",
			link: "https://github.com/BRO3886/google-tasks-cli",
		},
		{
			title: "Medium RSS API",
			subtitle: "A simple webservice to get JSON feed from Medium RSS Feed.",
			img: "assets/projects/medium-api.webp",
			redirect: "https://medium-rss-api.herokuapp.com/docs",
			github: "https://github.com/BRO3886/Medium-API",
			link: "https://medium-rss-api.herokuapp.com/docs",
		},
		{
			title: "Akina",
			subtitle: "Our contribution towards fighting the Covid-19 pandemic",
			img: "assets/projects/akina.webp",
			redirect: "https://youtu.be/rLHDL1hEmBw",
			github: "https://github.com/BRO3886/Project-Hestia",
			link: "https://akina.dscvit.com",
		},
		{
			title: "Katamari",
			subtitle: "A projects page generator for GitHub users and organisations",
			img: "assets/projects/katamari.webp",
			redirect: "https://github.com/BRO3886/katamari",
			github: "https://github.com/BRO3886/katamari",
			link: "https://github.com/BRO3886/katamari/releases",
		},
		{
			title: "Gin Template",
			subtitle: "A fully dockerised template repo for kickstarting Gin REST APIs",
			img: "assets/projects/gin-template.webp",
			redirect: "https://github.com/BRO3886/gin-template",
			github: "https://github.com/BRO3886/gin-template",
			link: "https://github.com/BRO3886/gin-template",
		},
		{
			title: "QwickScan",
			subtitle: "An app for making queues simpler during a pandemic.",
			img: "assets/projects/qwickscan.webp",
			redirect: "https://youtu.be/CCXDGFrspcs",
			github: "https://github.com/BRO3886/Apptitude-QwickScan",
			link: "https://github.com/BRO3886/qwickscan",
		},
	],
	work: [

		{
			title: "PT Paragon Technology and Innovation",
			url: "https://www.paragon-innovation.com/",
			img: "assets/work/paragon.png",
			duration: "Aug 2019 - Present",
			role: "Software Engineer - Solution Delivery and Management",
			about: [
				"Develope a website based application",
				"Tech Stack : HTML, CSS , JS , Yii2 , Bootstrap, React, Postgresql",
			],
		},

		{
			title: "PT Pakarti Tirtoagung",
			url: "http://www.pakarti.com/",
			img: "assets/work/pakarti.jpg",
			duration: "Jun 2018 - Aug 2018",
			role: "Eletrical Engineering Intern",
			about: [
				"Enter the control and instrumentation systems division.",
				"Assisting the control system upgrade project Electric Diesel Generator (EDG) Hangtuah from PLC 5 to PLC Controllogix 5000",
			],
		},


	],

	education: [

		{
			title: "Bandung Institute of Technology",
			url: "https://www.itb.ac.id/",
			img: "assets/work/itb.png",
			duration: "Jul 2015 - Aug 2019",
			role: "Electrical Engineering",
			about: [
				"Final project : ASSYST DRIVE | In-car assistive technology for drownsy driver.",
				"Activity : UPT Asrama ITB, HME ITB, LTPB ITB, Muslim STEI",
			],
		},

		{
			title: "MAN 02 Tulungagung",
			url: "https://man2tulungagung.sch.id/",
			img: "assets/work/man2.png",
			duration: "Jul 2012 - Mei 2015",
			role: "Natural Sciences",
			about: [
				"Activity : Marching Band and Math Olympic Team",

			],
		},


	],
};
