import React from "react";
import Link from "next/link";
import Header from "../components/Header";
import AboutMe from "../components/AboutMe";
import Nav from "../components/Nav";
import Footer from "../components/Footer";
import content from "../lib/content";
import { motion } from "framer-motion";

const About = () => {
	return (
		<div className="dark:bg-black">
			<Header />
			<Nav name="about" />
			<div className="w-10/12 mx-auto flex flex-col h-screen justify-center">
				<AboutMe />

			</div>
			<Footer />
		</div>
	);
};

export default About;
