import Link from "next/link";
import AboutMe from "../components/AboutMe";
import Arrow from "../components/Arrow";
import Footer from "../components/Footer";
import Header from "../components/Header";
import Landing from "../components/Landing";
import Nav from "../components/Nav";
import TechStack from "../components/TechStack";

export default function Home() {
	return (
		<div className="dark:bg-black dark:text-white antialiased">
			<Header />
			<Nav props="home" />
			<Landing />

			<div className="dark:bg-black" style={{ height: "10vh" }}></div>

			<Footer />
		</div>
	);
}
